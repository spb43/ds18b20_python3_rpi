# ds18b20_python3_rpi repository

Read temperature from ds18b20 sensor using python3


##  Enable 1-Wire interface on Rpi
Use command:
```
sudo raspi-config
```
Interface Options -> 1-Wire -> Enable and then reboot machine.

or you could add:
```
dtoverlay=w1-gpio
```
string to /boot/config.txt

After reboot device will be available on /sys/bus/w1/devices/

## Optional
If there are some problems with connection, check kernel modules are loaded:
```
lsmod | grep  w1
```
If they are not loaded, try to load:
```
sudo modprobe w1-gpio
sudo modprobe w1-therm
```
